<?php

use common\models\Book;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $books Book[] */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <?php Pjax::begin(['id' => 'cont-pj']); ?>
    <?php if (!empty($books)) {?>
    <div class="row">
        <?php  foreach ($books as $key => $book) { ?>
            <div class="col-3">
                <div class="book-cart">
                    <?php if (!empty($book->image) && file_exists(Yii::getAlias('@frontend').'/web/'.$book->image)) { ?>
                        <img class="book-cart__cover" src="<?= Yii::$app->request->baseUrl . $book->image ?>" alt="" >
                    <?php } else { ?>
                        <img class="book-cart__cover book-cart__cover--empty" src="" alt="" >
                    <?php } ?>

                    <h2 class="book-cart__title"><?= $book->name ?></h2>
                    <p class="book-cart__genres"><?= \implode(', ', \yii\helpers\ArrayHelper::map($book->getAllGenres(), 'name', 'name')) ?></p>
                    <?= \yii\helpers\Html::a('Подробнее', ['site/book', 'id' => $book->id], ['class' => 'book-cart__link btn btn-primary']) ?>

                </div>
            </div>
        <?php } ?>
    </div>

    <?= Html::hiddenInput('page', $page ?? 0, ['id' => 'page']) ?>
    <?= Html::label( 'Page size:', 'pagesize', array( 'style' => 'margin-left:10px; margin-top:8px;' ) ) ?>
    <?= Html::dropDownList('limit', $limit ?? 0,
            [ 16 => 16, 32 => 32, 48 => 48],
            ['id' => 'spjax']
    );
    ?>

        <?php
        $this->registerJs("
            $(function(){
              $('#spjax').on('change', function(){
                var Id = $(this).val();
                var page = $('#page').val();
              
                $.pjax.reload({
                    container: \"#cont-pj\",
                    url: '/',
                    timeout: 0,
                    data: {
                       'limit': Id ,
                       'page' : page 
                    },
                });
              })  
            })    
            ", View::POS_END)
        ?>

    <?= \yii\widgets\LinkPager::widget([
            'pagination' => $pages,

        ]);

        Pjax::end();
    } ?>

</div>
