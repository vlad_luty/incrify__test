<?php

use common\models\Book;
use common\models\Author;
use common\models\Genre;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\searches\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';
?>
<div class="site-index">
    <div class="body-content">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Авторы', ['author/index'], ['class' => 'btn btn-primary']) ?>&nbsp;&nbsp;<?= Html::a('Жанры', ['genre/index'], ['class' => 'btn btn-warning']) ?>
        </p>
        <p><?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?></p>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                [
                    'attribute' =>'public_at',
                    'format' => 'html',
                    'filter' => \yii\jui\DatePicker::widget([
                        'model'=>$model,
                        'attribute'=>'public_at',
                        'name' => 'BookSearch[public_at]',
                        'value' => ArrayHelper::getValue($_GET, "BookSearch.public_at"),
                        'language' => 'ru',
                        'dateFormat' => 'dd-MM-yyyy',
                        'options' => [
                             'class' => 'form-control'
                        ]
                    ]),
                    'value' => function($model){
                        /** @var $model Book */
                        return $model->getFormattedPublicAt();
                    },
                ],
                [
                    'attribute' =>'authorName',
                    'format' => 'raw',
                    'value' => function($model){
                        /** @var $model Book */
                        $authors = [];
                        foreach ($model->getAllAuthors() as $author) {
                            $authors[] = implode(' ', [$author->getFullName(),
                                Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['author/view', 'id' => $author->id]),
                                Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['author/edit', 'id' => $author->id]),
                                Html::a('<span class="glyphicon glyphicon-trash"></span>', ['author/delete', 'id' => $author->id])]);
                        }
                        return implode('<br/>', $authors);
                    },
                ],
                [
                    'attribute' =>'genre_id',
                    'format' => 'raw',
                    'value' => function($model){
                        /** @var $model Book */
                        $genres = [];
                        foreach ($model->getAllGenres() as $genre) {
                            $genres[] = $genre->name;
                        }
                        return implode(', ', $genres);
                    },
                    'filter' => ArrayHelper::map(Genre::find()->all(), 'id', 'name'),
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <?php Pjax::end(); ?>


    </div>
</div>
