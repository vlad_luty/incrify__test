<?php
namespace common\helpers;

/**
 * Класс-помощник для работы с временными метками
 * @package common\helpers
 */
class DateHelper
{
    const RUS_MONTH = [
        ['январь', 'января'],
        ['февраль', 'февраля'],
        ['март', 'марта'],
        ['апрель', 'апреля'],
        ['май', 'мая'],
        ['июнь', 'июня'],
        ['июль', 'июля'],
        ['август', 'августа'],
        ['сентябрь', 'сентября'],
        ['октябрь', 'октября'],
        ['ноябрь', 'ноября'],
        ['декабрь', 'декабря'],
    ];

    /**
     * Преобразование временной метки Unix в формат 99 <название месяца>
     * @param $date integer Unix-время
     * @return int
     */
    public static function asRusDate($unixTime) {
        $day = date('j', $unixTime);
        $month = static::RUS_MONTH[date('n', $unixTime) - 1][1];
        $year = (int)date('Y', $unixTime);

        return sprintf("%s %s %s", $day, $month, $year);
    }

}