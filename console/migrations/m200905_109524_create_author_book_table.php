<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%author_book}}`.
 */
class m200905_109524_create_author_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%author_book}}', [
            'id'        => $this->primaryKey(),
            'author_id' => $this->integer(11)->notNull()->comment('ПК Автора'),
            'book_id'   => $this->integer(11)->notNull()->comment('ПК Книги')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%author_book}}');
    }
}
