<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%author}}`.
 */
class m200905_103900_create_author_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%author}}', [
            'id'         => $this->primaryKey(),
            'first_name' => $this->string('80')->notNull()->comment('Имя автора'),
            'last_name'  => $this->string('80')->notNull()->comment('Фамилия автора'),
            'birth_day'  => $this->integer(11)->notNull()->comment('Дата рождения'),
            'phone'      => $this->string(20)->comment('Номер телефона')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%author}}');
    }
}
